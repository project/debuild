<?php
// $Id: git.inc,v 1.6 2010/04/02 04:06:38 weitzman Exp $

/**
 * @file Drush pm Git extension
 */

class drush_pm_version_control_git implements drush_pm_version_control {

  private $statuses = array(
     'M' => 'updated in index',
     'A' => 'added to index',
     'D' => 'deleted from index',
     'R' => 'renamed in index',
     'C' => 'copied in index',
     'DD' => 'unmerged, both deleted',
     'AU' => 'unmerged, added by us',
     'UD' => 'unmerged, deleted by them',
     'UA' => 'unmerged, added by them',
     'DU' => 'unmerged, deleted by us',
     'AA' => 'unmerged, both added',
     'UU' => 'unmerged, both modified',
     '??' => 'untracked',
  );

  protected $repo_root;

  public function __construct() {
    $this->repo_root = drush_get_context('DRUSH_DRUPAL_ROOT');
  }

  /**
   * Git the current git commit has.
   */
  public function hash() {
    if (!drush_shell_exec("cd %s && git log -n 1 | grep -e ^commit | tr -d commit\ ", $this->repo_root)) {
      return FALSE;
    }
    return trim(implode('', drush_shell_exec_output()));
  }

  /**
   * Get the root of the repository.
   */
  protected function getRepo($path = FALSE) {
    if (!$path) $path = drush_get_context('DRUSH_DRUPAL_ROOT');
    if (!drush_shell_exec('cd ' . $path . ' && readlink -f ./$(git rev-parse --show-cdup)')) {
      return FALSE;
    }
    return $this->repo_root = trim(implode('', drush_shell_exec_output()));
  }

  /**
   * Implementation of type().
   */
  function name() {
    return 'git';
  }

  function getUsername() {
    return exec('git config user.name');
  }

  function getEmail() {
    return exec('git config user.email');
  }

  /**
   * Retrieve the current branch git is on.
   */
  public function getBranch() {
    if (!drush_shell_exec("cd %s && git branch 2>/dev/null|grep -e ^* | tr -d \*\ ", $this->repo_root)) {
      return FALSE;
    }
    return trim(implode('', drush_shell_exec_output()));
  }

  /**
   * Implementation of tag().
   */
  public function tag($tag_name) {
    if (!$this->sync()) {
      return drush_set_error(dt("Unable to tag current state in Git. Sync failed."));
    }
    if (!drush_shell_exec('git tag %s', $tag_name)) {
      return drush_set_error(implode(PHP_EOL, drush_shell_exec_output()));
    }
    drush_print(dt("tag '!tag' successfully tagged", array('!tag' => $tag_name)));
    return TRUE;
  }

  /**
   * Implementation of export().
   */
  public function export($branch, $destination) {
    if (is_dir($destination)) {
      if (drush_confirm("$destination exists. Do you want to remove it?")) {
        if (!drush_shell_exec('rm -rf %s', $destination)) {
          return drush_set_error(dt('!dir already exists and cannot be removed.', array('!dir' => $destination)));
        }
        if (!mkdir($destination, 0766)) {
          return drush_set_error(dt("Cannot make directory !dir", array('!dir' => $destination)));
        }
        return drush_shell_exec('git archive %s | tar -x -C %s', $branch, $destination);
      }
      return drush_set_error("Cannot export $branch to $destination");
    }
    if (!mkdir($destination, 0766)) {
      return drush_set_error(dt("Cannot make directory !dir", array('!dir' => $destination)));
    }
    return drush_shell_exec('git archive %s | tar -x -C %s', $branch, $destination);
  }

  /**
   * Retrieve Git status.
   */
  public function status($path = '') {
    if ($path) {
      $path = str_replace($this->repo_root . '/', '', $path);
    }
    if (!drush_shell_exec('cd %s && git status --porcelain ' . $path, $this->repo_root)) {
      return drush_set_error('DRUSH_PM_GIT_NOT_FOUND', dt("Git status command failed. Are you within the git root?"));
    }
    return drush_shell_exec_output();
  }

  /**
   * Implementation of pre_update().
   */
  public function pre_update(&$project, $items_to_test = array()) {
    $rows = array();
    foreach ($this->status($project['path']) as $line) {
      list($status, $filepath) = explode(' ', trim($line));
      $rows = array($status, $filepath, $this->statuses[$status]);
    }
    if (!empty($rows)) {
      return drush_set_error(dt("There are uncommented changes in !path. Please commit or revert these changes before continuing:\n!output", array(
        '!output' => drush_print_table($rows),
        '!path' => $project['path'],
      )));
    }
    // By the distrobuted nature of Git, we shouldn't pull from a remote if any. The user can do that.
    return TRUE;
  }

  /**
   * Implementation of rollback().
   */
  public function rollback($project) {
    $branch = $this->getBranch();
    foreach ($this->status($project['path']) as $line) {
      list($status, $filepath) = explode(' ', trim($line));
      drush_shell_exec('cd %s && git checkout ' . $branch . ' ' . $filepath, $this->repo_root);
    }
    return TRUE;
  }

  /**
   * Automatically add any unversioned files to Git and remove any files
   * that have been deleted on the file system ready to be commited.
   */
  private function sync($path = FALSE) {
    $path = $path ? $path : $this->repo_root;
    $files = array();
    foreach ((array) $this->status($path) as $line) {
      list($status, $filepath) = explode(' ', trim($line));
      $status = trim($status);
      switch ($status) {
        case '??':
          if (drush_get_option('gitforce') || drush_confirm(dt("!file is untracked. Would you like to add it to Git? (use --gitforce to prevent this message)", array('!file' => $filepath)))) {
            $files['add'][] = $filepath;
          }
        break;
        case 'AM':
        case 'M':
        case 'A':
        case 'R':
          $files['add'][] = $filepath;
        break;
        case 'D':
          $files['rm'][] = $filepath;
        break;
        default:
          if (isset($this->statuses[$status])) {
            return drush_set_error(dt('Unexpected status message: !state: !file', array('!state' => $this->statuses[$status], '!file' => $filepath)));
          }
        break;
      }
    }
    if (empty($files)) {
      return TRUE;
    }
    foreach ($files as $command => $paths) {
      $paths = array_filter($paths);
      if (empty($paths)) {
        continue;
      }
      if (!drush_shell_exec('git ' . $command . ' ' . implode(' ', $paths))) {
        return drush_set_error('DRUSH_PM_GIT_SYNC_PROBLEMS', implode(PHP_EOL, drush_shell_exec_output()));
      }
    }
    // Git is ready to commit.
    return TRUE;
  }

  /**
   * Implementation of post_update().
   */
  public function post_update($project) {
    if (drush_get_option('gitcommit')) {
      if (!$this->sync($project['path'])) {
        return drush_set_error(dt("Failed to commit changes from !path", array('!path' => $project['path'])));
      }
      // Only attempt commit on a sucessful sync
      return $this->commit($project);
    }
    return TRUE;
  }

  /**
   * Implementation of post_download().
   */
  public function post_download($project) {
    if (drush_get_option('gitcommit')) {
      if (!$this->sync($project['path'])) {
        return drush_set_error(dt("Cannot add downloaded changes to !path", array('!path' => $project['path'])));
      }
      // Only attempt commit on a sucessful sync
      return $this->commit($project);
    }
    return TRUE;
  }

  /**
   * Automatically commit changes to the repository
   */
  public function commit($project) {
    $message = drush_get_option('gitmessage');
    if (empty($message)) {
      $message = dt("Drush commit. Changes to !path", array('!path' => $project['path']));
    }
    if (drush_shell_exec('git commit -a -m %s', $message)) {
      drush_log(dt('Project committed to Git successfully'), 'ok');
      return TRUE;
    }
    return drush_set_error('DRUSH_PM_GIT_COMMIT_PROBLEMS', dt("Problems were encountered committing your changes to Git.\nThe specific errors are below:\n!errors", array('!errors' => implode("\n", drush_shell_exec_output()))));
  }

  public static function reserved_files() {
    return array('.git', '.gitignore', '.gitsubmodules');
  }
}
