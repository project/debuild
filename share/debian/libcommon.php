<?php

/**
 * No Change to a templated file.
 */
define('TEMPLATE_NO_CHANGE', 100);

/**
 * Template file changed.
 */
define('TEMPLATE_CHANGE', 200);

/**
 * Print out string to output.
 */
function deb_print($text) {
  $args = func_get_args();
  echo call_user_func_array('sprintf', $args) . PHP_EOL;
}

/**
 * Export database settings for the format appropriate to the Drupal version.
 */
function deb_export_db_settings($databases) {
  switch (deb_drupal_version()) {
    case 7:
      return '$databases = ' . deb_var_export($databases) . ';' . PHP_EOL;
    break;
    case 6:
    case 5:
      $db_string = '';
      foreach ($databases as $db_id => $dbs) {
        // If there is no password, the value 'password' will be removed leaving
        // the colon ':' next to the at symbol (@). Since there would be no
        // password. It will be fine to remove it.
        $url = strtr('driver://username:password@host:port/database', $dbs['default']);
        // As with no password, if there is no port specified, then the colon
        // after the host should be removed. The alphabetic character is searched
        // to ensure the protocol doesn't match (://).
        $url = preg_replace('/\:\/(\w)/', '/$1', $url);

        $db_string .= '$db_url["' . $db_id . '"] = "' . $url . '";' . PHP_EOL;
      }

      return $db_string;
    break;
    default:
      throw new Exception("Unknown version of Drupal. Drupal " . deb_drupal_version());
    break;
  }
}

/**
 * Template a file and copy it to its location.
 *
 * @see apache.tpl.php.
 */
function deb_template_file($tpl, $vars, $dest) {
  $data = deb_template($tpl, $vars);
  if (file_exists($dest) && md5(file_get_contents($dest)) == md5($data)) {
    return TEMPLATE_NO_CHANGE;
  }
  return file_put_contents($dest, $data) ? TEMPLATE_CHANGE : FALSE;
}

/**
 * Template a file and return the result.
 */
function deb_template($tpl, $vars) {
  extract($vars);
  ob_start();
  include $tpl;
  $contents = ob_get_contents();
  ob_end_clean();
  return $contents;
}

/**
 * Find the major version of Drupal.
 */
function deb_drupal_version() {
  if (!defined('VERSION')) {
    include_once WEBROOT . '/modules/system/system.module';

    // Drupal 7's version constant is bootstrap.inc
    if (!defined('VERSION')) {
      include_once WEBROOT . '/includes/bootstrap.inc';
    }
    if (!defined('VERSION')) {
      throw new Exception("Unknown version of Drupal. Cannot determine which version of Drupal is running.");
    }
  }
  list($major, $minor) = explode('.', VERSION);
  return $major;
}

/**
 * Export var function -- from Views.
 */
function deb_var_export($var, $prefix = '', $init = TRUE) {
  if (is_object($var)) {
    $output = method_exists($var, 'export') ? $var->export() : deb_var_export((array) $var);
  }
  else if (is_array($var)) {
    if (empty($var)) {
      $output = 'array()';
    }
    else {
      $output = "array(\n";
      foreach ($var as $key => $value) {
        $output .= "  '$key' => " . deb_var_export($value, '  ', FALSE) . ",\n";
      }
      $output .= ')';
    }
  }
  else if (is_bool($var)) {
    $output = $var ? 'TRUE' : 'FALSE';
  }
  else if (is_string($var) && strpos($var, "\n") !== FALSE) {
    // Replace line breaks in strings with a token for replacement
    // at the very end. This protects whitespace in strings from
    // unintentional indentation.
    $var = str_replace("\n", "***BREAK***", $var);
    $output = var_export($var, TRUE);
  }
  else {
    $output = var_export($var, TRUE);
  }

  if ($prefix) {
    $output = str_replace("\n", "\n$prefix", $output);
  }

  if ($init) {
    $output = str_replace("***BREAK***", "\n", $output);
  }

  return $output;
}

/**
 * Executes a shell command.
 * Output is only printed if in verbose mode.
 * Output is stored and can be retrieved using drush_shell_exec_output().
 * If in simulation mode, no action is taken.
 *
 * @param $cmd
 *   The command to execute. May include placeholders used for sprintf.
 * @param ...
 *   Values for the placeholders specified in $cmd. Each of these will be passed through escapeshellarg() to ensure they are safe to use on the command line.
 * @return
 *   0 if success.
 */
function deb_shell_exec($cmd) {
  $args = func_get_args();

  //do not change the command itself, just the parameters.
  for ($x = 1; $x < sizeof($args); $x++) {
    $args[$x] = escapeshellarg($args[$x]);
  }
  $command = call_user_func_array('sprintf', $args);

  exec($command . ' 2>&1', $output, $result);
  deb_shell_exec_output($output);

  // Exit code 0 means success.
  return ($result == 0);
}

/**
 * Stores output for the most recent shell command.
 * This should only be run from drush_shell_exec().
 *
 * @param $output
 *   The output of the most recent shell command.
 *   If this is not set the stored value will be returned.
 */
function deb_shell_exec_output($output = FALSE) {
  static $stored_output;
  if ($output === FALSE) return $stored_output;
  $stored_output = $output;
}

/**
 * Create a Drupal webuser.
 *
 * Simple function that creates a system user with home directory
 * bash default shell a group with the same name and attempts to
 * reserve a common uid in a load balanced environment.
 */
function deb_create_drupal_user() {
  $uid_file = SITE_LIB . '/' . PACKAGE_NAME . '.uid';
  // In a load balanced environment the webuser must have the same uid
  // on all webservers. The first server to install this package will
  // create the user and store the uid in a file in
  // /var/lib/sitedata/__PACKAGE__ so that the the following servers
  // can use that file to determine the uid to use when creating the user.
  $uid = file_exists($uid_file) ? file_get_contents($uid_file) : FALSE;

  if (!$uid) {
    // Drupal users should have a uid higher >= 500.
    $uid = 500;
    deb_shell_exec('cat /etc/passwd | grep drupal-site');
    foreach (deb_shell_exec_output() as $line) {
      $line = explode(':', $line);
      if ($line[2] >= $uid) {
        $uid = $line[2] + 1;
      }
    }
  }

  if (!deb_shell_exec('adduser --system --group --home %s --shell /bin/bash --uid ' . $uid . ' %s', WEBROOT, WEBUSER)) {
    deb_print("An error occured while trying to create the webuser.");
    deb_print(implode(PHP_EOL, deb_shell_exec_output()));
    exit(2);
  }
  deb_print(implode(PHP_EOL, deb_shell_exec_output()));
  // Ensure the webuser owns the home directory. It may not if this package is
  // being reinstalled.
  chown(WEBROOT, WEBUSER);
  chgrp(WEBROOT, WEBUSER);

  // Store the uid for other web servers to use.
  if (!file_exists($uid_file) && !file_put_contents($uid_file, $uid)) {
    deb_print("An error occured while trying to create the webuser. Could not store the webuser's uid $uid.");
  }
  return $uid;
}
/**
 * Try to obtain a lock across a load balanced environment.
 *
 * The assumption is that SITE_LIB is shared in realtime
 * across all load balanced servers.
 *
 * @param namespace
 *    Namespace to allow the use of locks for several tasks.
 */
function deb_obtain_lock($ns) {
  $lock = SITE_LIB . '/' . PACKAGE_NAME . '.' . $ns;
  if (file_exists($lock)) {
    return FALSE;
  }
  register_shutdown_function('deb_release_lock', $ns);
  return file_put_contents($lock, time());
}

/**
 * Release an obtained lock.
 *
 * Warning: this may release a lock not obtained by said server.
 */
function deb_release_lock($ns) {
  $lock = SITE_LIB . '/' . PACKAGE_NAME . '.' . $ns;
  return !file_exists($lock) || unlink($lock);
}

/**
 * Halt PHP until the lock can be obtained or timeout.
 */
function deb_wait_for_lock($ns) {
  // Set timeout at a minute.
  $timeout = time() + 60;

  // Exit loop once a lock is obtained or timeout occurs.
  while ($timeout > time()) {
    if (deb_obtain_lock($ns)) {
      return TRUE;
    }
    deb_print("Another server is running the configuration process. Sleeping for 5 seconds....");
    sleep(5);
  }

  // If timeout didn't occur than obtaining the lock
  // as successful.
  return FALSE;
}

/**
 * Configure Main Cron Task.
 */
function deb_cron_conf($debconf) {
  $cron_conf = array();
  $offset = crc32(PACKAGE_NAME) % CRON_FREQUENCY;

  // $offset maybe negative so do a quick bit of math
  // to re-evaluate it as positive.
  $offset = sqrt($offset * $offset);

  $cron_conf['minutes'] = $offset . '-59/' . CRON_FREQUENCY;

  // Set up cron line: drush -r /var/www/drupal-site-[project] -u 1 -l http://[servername] cron
  // aexec is a atomic execution tool will ensure cron is only run once in multiserver environments.
  $cmd = exec('which aexec');
  if (!empty($cmd)) {
    $cmd .= ' -d ' . rand(1,59) . ' -f /var/lib/sitedata/' . PACKAGE_NAME . '/cron.lock -v ';
  }
  else {
    deb_print("Please install aexec to enable atomic cron runs in cluster environments.");
  }
  $cron_conf['cmd'] = $cmd . 'drush -r /var/www/' . PACKAGE_NAME . ' -u 1 -l http://' . $debconf->get('servername') . ' --nocolor --quiet cron >> ' . LOG_DIR . '/cron.log';
  $cron_conf['servername'] = $debconf->get('servername');

  return $cron_conf;
}
