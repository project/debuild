<?php

/**
 * Make the debian 'templates' file.
 */
function dh_make_templates_file($contents = '') {
  foreach (drush_get_option_list('cfgmgr-services', array()) as $namespace) {
    list($style, $type, $service) = explode('.', $namespace);
    if (!$service_info = cfgmgr_registry_load($service, $type, $style)) {
      drush_log('Could not load service from namespace: ' . $namespace, 'warning');
      continue;
    }
    if (!$service = cfgmgr_load_service($service_info)) {
      continue;
    }
    foreach ($service->cfg()->registry_variables() as $key => $conf) {
      $contents .= PHP_EOL . 'Template: __PACKAGE__/' . $service_info['service'] . '_' . $key . PHP_EOL;
      switch ($conf['#type']) {
        case 'ask':
          $contents .= 'Type: string' . PHP_EOL;
          break;
        case 'select':
          $contents .= 'Type: select' . PHP_EOL;
          $contents .= 'Choices: ';
          $choices = array();
          foreach ($conf['#options'] as $choice) {
            $choices[] = $choice['!key']; 
          }
          $contents .= implode(', ', $choices) . PHP_EOL;
          break;
        case 'confirm':
          $contents .= 'Type: boolean' . PHP_EOL;
          break;
        case 'notify':
          $contents .= 'Type: note' . PHP_EOL;
          break;
      }
      if (isset($conf['#default'])) {
        $contents .= 'Default: ' . $conf['#default'] . PHP_EOL;
      }
      $contents .= 'Description: ' . $conf['#question'] . PHP_EOL;
    }
  }
  // Legacy module implements way of adding to templates.
  foreach (module_implements('debian_templates') as $module) {
    $contents .= PHP_EOL . module_invoke($module, 'debian_templates');
  }
  
  return $contents;
}

/**
 * Make the debian 'config' file.
 */
function dh_make_config_file($contents) {
  $code = PHP_EOL;
  $code .= 'if [ -d __WWWROOT__ ]; then' . PHP_EOL;
  

  // New system to allow drush integration.
  $code .= '  drush -r __WWWROOT__ dpkg-config __PACKAGE__ || true' . PHP_EOL;
  $code .= 'fi' . PHP_EOL;

  // Support legacy systems.
  $code .= PHP_EOL;
  foreach (module_implements('debian_config') as $module) {
    $code .= "# Implemented by $module." . PHP_EOL;
    $code .= module_invoke($module, 'debian_config') . PHP_EOL;
    $code .= "# Implementation by $module finished." . PHP_EOL;
  }
  return strtr($contents, array(
    '#__MODULE_HOOK__' => $code,
    '#__SSL_SUPPORT__' => 'SSL_SUPPORT=' . (drush_get_option('dh-build-ssl', FALSE) ? '1' : '0') . PHP_EOL,
  ));
  return $contents;
}

/**
 * Make the debian 'dirs' file.
 */
function dh_make_dirs_file($contents) {
  // Dirs is already configured.
  return $contents;
}

/**
 * Generate debian file 'postinst'.
 */
function dh_make_postinst_file($contents) {
  $contents = <<<EOF
#!/bin/sh
# postinst script for drupal package
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <postinst> `abort-remove'
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

case "$1" in
    configure)
      # if the reset flag file is present, reset all the
      # debconf variables to be reasked.
      #if [ -f /tmp/__PACKAGE__.reset ]; then
      #  drush --cfg_handler=debconf --ns=__PACKAGE__ cfgmgr-reset
      #  rm /tmp/__PACKAGE__.reset
      #fi

EOF;
  $contents .= '    # Arguments #' . PHP_EOL;
  $contents .= '    # -r              webroot' . PHP_EOL;
  $contents .= '    # --cfg-hander    configuration handler' . PHP_EOL;
  $contents .= '    # --ns            namespace (dpkg name)' . PHP_EOL;
  $contents .= '    # --confdir       basepath to place all configuration relative to.' . PHP_EOL;
  $contents .= '    # --fire-events   fire any "on change" events when configuration ' . PHP_EOL;
  $contents .= '    #                 changes such as restart nginx when a vhost conf file changes.' . PHP_EOL;
  $contents .= '    # --exec          execute service commands instead of just printing them.' . PHP_EOL;
  $contents .= '    # --install       run install events if present.' . PHP_EOL;
  foreach (drush_get_option_list('cfgmgr-services', array()) as $namespace) {
    list($style, $type, $service) = explode('.', $namespace);
    if (!$service = cfgmgr_registry_load($service, $type, $style)) {
      drush_log('Could not load service from namespace: ' . $namespace, 'warning');
      continue;
    }
    $contents .= '     # Implemented by service ' . $service['service'] . PHP_EOL;
    $contents .= '     drush -r __WWWROOT__ --cfg_handler=debconf --ns=__PACKAGE__ --confdir=/ --fire-events=1 --exec=1 --install=1 cfgmgr ' . $namespace . PHP_EOL;
  }
  $version = drush_get_option('dh-version');

$contents .=<<<EOF
      if [ -d __WWWROOT__ ]; then
        drush -r __WWWROOT__ vset --yes dpkg_version '$version' || true
        drush -r __WWWROOT__ dpkg-postinst __PACKAGE__ $1 || true
      fi
    ;;
    abort-upgrade|abort-remove|abort-deconfigure)
      if [ -d __WWWROOT__ ]; then
        drush -r __WWWROOT__ dpkg-postinst __PACKAGE__ $1 || true
      fi
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
EOF;
  return $contents;
}

/**
 * Generate debian file 'preinst'.
 */
function dh_make_preinst_file($contents) {
  $contents = <<<EOF
#!/bin/bash
# preinst script for drupal package
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <new-preinst> `install'
#        * <new-preinst> `install' <old-version>
#        * <new-preinst> `upgrade' <old-version>
#        * <old-preinst> `abort-upgrade' <new-version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

. /usr/share/debconf/confmodule

case "$1" in
    install)
    ;;
    upgrade|abort-upgrade)
      if [ -d __WWWROOT__ ]; then
        drush -r __WWWROOT__ dpkg-preinst __PACKAGE__ $1 || true
      fi
      # If the reset flag file is set (@see prerm) then remove it
      # to prevent postinst from reseting all variables.
      #if [ "$1" == "upgrade" ] && [ -f /tmp/__PACKAGE__.reset ]; then
      #  rm /tmp/__PACKAGE__.reset
      #fi
    ;;

    *)
        echo "preinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
EOF;
  return $contents;
}

/**
 * Generate debian file 'prerm'.
 */
function dh_make_prerm_file($contents) {
  $contents = <<<EOF
#!/bin/bash
# prerm script for drupal package
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <prerm> `remove'
#        * <old-prerm> `upgrade' <new-version>
#        * <new-prerm> `failed-upgrade' <old-version>
#        * <conflictor's-prerm> `remove' `in-favour' <package> <new-version>
#        * <deconfigured's-prerm> `deconfigure' `in-favour'
#          <package-being-installed> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package


case "$1" in
    remove|upgrade|deconfigure|failed-upgrade)
      if [ -d __WWWROOT__ ]; then
        drush -r __WWWROOT__ dpkg-prerm __PACKAGE__ $1 || true
      fi
      # if the action id to remove, then set the variables to
      # be reset. As preinst on runs on install and not reconfigure
      # removing this file in preinst will allow reconfigure scripts
      # to ask questions managed by drush again.
      #if [ "$1" == "upgrade" ] && [ ! -f /tmp/__PACKAGE__.reset ]; then
      #  touch /tmp/__PACKAGE__.reset
      #fi
    ;;

    *)
        echo "prerm called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
EOF;
  return $contents;
}

/**
 * Generate debian file 'postrm'.
 */
function dh_make_postrm_file($contents) {
  $contents = <<<EOF
#!/bin/sh
# postrm script for drupal package
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <postrm> `remove'
#        * <postrm> `purge'
#        * <old-postrm> `upgrade' <new-version>
#        * <new-postrm> `failed-upgrade' <old-version>
#        * <new-postrm> `abort-install'
#        * <new-postrm> `abort-install' <old-version>
#        * <new-postrm> `abort-upgrade' <old-version>
#        * <disappearer's-postrm> `disappear' <overwriter>
#          <overwriter-version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package


case "$1" in
    purge|remove|upgrade|failed-upgrade|abort-install|abort-upgrade|disappear)
      if [ -d __WWWROOT__ ]; then
        drush -r __WWWROOT__ dpkg-postrm __PACKAGE__ $1 || true
      fi
    ;;

    *)
        echo "postrm called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
EOF;
  return $contents;
}

/**
 * Generate debian file 'control'.
 */
function dh_make_control_file($contents) {
  $control = array(
    'Source' => drush_get_option('dh-package'),
    'Section' => 'web',
    'Priority' => 'optional',
    'Package' => drush_get_option('dh-package'),
    'Architecture' => 'all',
    'Depends' => array('${misc:Depends}', 'apache2 | nginx', 'cron', 'php5-pgsql | php5-mysql', 'mysql-client | postgresql-client', 'libapache2-mod-php5 | php5-cgi | php5-fpm'),
    'Suggests' => array(),
    'Pre-Depends' => array('php5-cli', 'drush'),
    'Description' => dt('Drupal !version site.', array(
      '!package' => drush_get_option('dh-package'),
      '!user' => drush_get_option('dh-user'),
      '!version' => drush_drupal_major_version())
    ),
  );

  $contents = file_get_contents(drush_get_option('dh-build-dir') . '/debian/control');

  drupal_alter('debian_control', $control);

  foreach ($control as $key => $value) {
    if (is_array($value)) {
      $value = implode(', ', $value);
    }
    $contents = preg_replace("/^$key:\s([^\n]+)\n/m", "$key: $value\n", $contents);
  }
  return strtr($contents, array('<insert long description, indented with spaces>' => dt('Package maintained by !user', array('!user' => drush_get_option('dh-user')))));
}


/**
 * Generate debian file 'changelog'.
 */
function dh_make_changelog_file($contents) {
  return file_get_contents(DRUPAL_ROOT . '/.debian/changelog');

}

function dh_update_changelog_file() {
  $change_log = array();
  $vc_engine = drush_pm_include_version_control(DRUPAL_ROOT);
  // With version control, get the last date the changelog changed.
  // This will give us a start point to populate the changelog with.

  if (file_exists(DRUPAL_ROOT . '/.debian/changelog')) {
    $contents = file_get_contents(DRUPAL_ROOT . '/.debian/changelog');
    if (!drush_shell_exec('cd %s && git log -n 1 --diff-filter=ACDMRT %s', DRUPAL_ROOT, '.debian/changelog')) {
      return drush_set_error("Unable to create changelog. Could not get Git log on file. Maybe you need to commit .debian/changelog to version control first?");
    }
    $commit = drush_shell_exec_output();
    list($text, $hash) = explode(' ', $commit[0], 2);

    // Retrieve the git log since changelog was last updated.
    drush_shell_exec('cd %s && git log --date-order --pretty=format:\'%%aN: %%s\' %s..%s', DRUPAL_ROOT, $hash, drush_get_option('dh-git-tag', $vc_engine->getBranch()));
    foreach (drush_shell_exec_output() as $line) {
      $change_log[] = $line;
    }
  }
  else {
    $contents = '';
    $change_log[] = dt("Debianized Drupal project with drush dh-make api: " . DRUSH_DH_MAKE_API);
  }

  // Automate the versioning if specified.
  $date = date('Y.m.d');
  $suffix = 1;
  if (preg_match_all('/' . preg_quote($date) . '-(\d+)/', $contents, $matches)) {
    foreach ($matches[1] as $i) {
      $suffix = ($i > $suffix) ? $i : $suffix;
    }
    $suffix++;
  }
  drush_set_option('dh-version', $date . '-' . $suffix . drush_get_option('dh-version-suffix', ''));
  

  // If there are no new logs, as in no newer commits since our
  // Drupal project last had a package built, then there is no
  // need to bump the changelog.
  if (empty($change_log)) {
    // Decrement the version as we're not bumping the changelog.
    $suffix = ($suffix == 1) ? $suffix : --$suffix;
    drush_set_option('dh-version', $date . '-' . $suffix . drush_get_option('dh-version-suffix', ''));
    return FALSE;
  }

  // Turn commas into bullet points.
  foreach (module_implements('debian_changelog') as $module) {
    $change_log += module_invoke($module, 'debian_changelog');
  }
  // Allow the changelog to be altered before writting it to file.
  drupal_alter('debian_changelog', $change_log);

  $logs = '  * ' . implode(PHP_EOL . '  * ', $change_log) . PHP_EOL;

  $entry = drush_get_option('dh-package') . ' (' . drush_get_option('dh-version') . ') unstable; urgency=low' . PHP_EOL . PHP_EOL;
  $entry .= $logs . PHP_EOL;
  $entry .= ' -- ' . drush_get_option('dh-user') . ' <' . drush_get_option('dh-email') . '>  ' . date('D, j M Y H:i:s O') . PHP_EOL . PHP_EOL;

  // Write the new changelog to file
  file_put_contents(DRUPAL_ROOT . '/.debian/changelog', $entry . $contents);

  // Set changelog as a global to allow other modules to use it.
  drush_set_option('debian_changelog', $change_log);

  // Return the changelog to the new build directory.
  return TRUE;
}

/**
 * Generate debian file 'install'.
 */
function dh_make_install_file($contents) {
  if (!$dh = opendir(drush_get_option('dh-build-dir'))) {
    drush_set_error("Cannot open drupal root directory.");
    return;
  }
  $www = 'var/www/' . drush_get_option('dh-package');
  $install = array();
  // Go through the root directory and include all files and directories in the install file.
  $excluded_files = array(
    '.', '..', '.htaccess', 'sites', '.debian', 'debian', 'Makefile',
    'update.php', 'install.php', 'cron.php', 'CHANGELOG.txt',
  );
  while (($file = readdir($dh)) !== false) {
    // Exclude these files and folders, they should not be in the production docroot.
    if (in_array($file, $excluded_files)) {
      continue;
    }

    $install[] = array($file, $www);
  }
  closedir($dh);

  // The sites folder is excluded so we need to handle it a bit differently.
  $install[] = array('sites/all', $www . '/sites');
  if (file_exists(drush_get_option('dh-build-dir') . '/sites/default/settings.common.php')) {
    $install[] = array('sites/default/settings.common.php', $www . '/sites/default');
  }

  // Put changelog and default.settings.php in /etc
  $install[] = array('CHANGELOG.txt', 'etc/__PACKAGE__');
  $install[] = array('sites/default/default.settings.php', '__WWWROOT__/sites/default');

  // Add libraries we'll need.
//  $install[] = array('debian/libdebconf.php', 'usr/share/' . drush_get_option('dh-package'));
//  $install[] = array('debian/libcommon.php', 'usr/share/' . drush_get_option('dh-package'));

  // Allow modules to add install files.
  foreach (module_implements('debian_install') as $module) {
    $install = array_merge($install, module_invoke($module, 'debian_install'));
  }

  $colen = 0;
  // Determine the max length required to make a column based structure to the files.
  foreach ($install as $line) {
    $colen = strlen($line[0]) > $colen ? strlen($line[0]) : $colen;
  }
  foreach ($install as &$line) {
    $line[0] = str_pad($line[0], $colen);
    $line = implode(' ', $line);
  }
  return implode(PHP_EOL, $install) . PHP_EOL;
}
