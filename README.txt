=Drush dh-make-drupal (api 2)=

== About ==
dh-make-drupal 2 is a more flexible and complex tool
to build debian packages from a drupal instance. It relies
on a couple of drush extensions and Git to be able to 
build a debian package.

dh-make-drupal is designed to exist on a development
environment where the debian package will be built. It
does not need to exist where the debian package will be deployed.

== Installation ==
 * Ubuntu 10.04 LTS or greater; or
 * Debian Squeeze or later
=== Requirements===
- Development Environment
  * dh-make 0.58 or greater
  * devscripts 2.10 or greater
  * Git 1.7 or greater
  * Drush 4.4.2 or greater (not tested with versions lower)
  * Drush Configuration Manager (http://drupal.org/project/cfgmgr)
  * Drush dpkg 7.x-1.0 or greater (http://drupal.org/project/dpkg)
  * Drush dh-make-drupal (this package)
- Deployable Environment (such as production)
  * Drush 4.4.2 or greater (not tested with versions lower)
  * Drush Configuration Manager (http://drupal.org/project/cfgmgr)
  * Drush dpkg 7.x-1.0 or greater (http://drupal.org/project/dpkg)

See http://drupal.org/project/drush for more details on how to install
drush commands.

== Usage ==
dh-make-drupal will simply bundle up the code base and deploy it to what ever
servers you deploy it to, assuming they are of debian decent. By default, dh-make-drupal
will also place drush dpkg commands in the preinst, postinst, prerm, postrm and config
scripts so that other commands and modules make take advantage of the installation process.
Example:
  drush dh-make-drupal --dh-api=2

dh-make-drupal gets better when you combine it with cgfmgr - a configuration management
tool that will generate configuration files, using the cfgmgr-services parameter, you
can add additional supported configuration files to the debian package to be deployed
also. dh-make-drupal will also enable cfgmgr's debconf cfg handler which will prompt
the user when installing the package for varous configuration options.
Example:
  # Install drupal, connect it to the database, put the sites/default/files on a seperate partition and configure an nginx vhost.
  drush dh-make-drupal --dh-api=2 --cfg-services=conf.settings.drupal,conf.vhost.nginx 
