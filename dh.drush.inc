<?php
/**
 * @file
 *  The drush debian helper
 *
 */

define('DRUSH_DH_MAKE_API', 2);

/**
 * Implementation of hook_drush_command().
 */
function dh_drush_command() {
  $items['dh-make-drupal'] = array(
    'description' => 'Debianize Drupal into a buildable debian package.',
    'arguments' => array(),
    'callback' => 'drush_dh_make_drupal',
    'aliases' => array('dh-make', 'dmd'),
    'options' => array(
      'dh-email' => 'Email address to use for the changelog',
      'dh-user' => 'Username to use for the changelog',
      'dh-project' => 'Project name. Defaults to root folder name',
      'dh-package' => 'Package name. Defaults to drupal-site-[dh-project]',
      'dh-build-dir' => 'Location to build the package. This location will be purged if exists.',
      'dh-make-depends' => 'An array of modules that must be enabled',
      'dh-make-recommends' => 'An array of modules to recommend. Issues warnings if not enabled',
      'dh-make-conflicts' => 'Modules that must not be enabled. Build will fail otherwise',
      'dh-version-suffix' => 'A suffix to append to the version of a bumped debian package',
      'cfgmgr-services' => 'a comma seperated list of services to compile with the packaging. List is available with "drush cfgmgr-list"',
    ),
    'examples' => array(
      'drush dmd --cfgmgr-services=conf.vhost.nginx' => 'Creates a debian package with a Nginx vhost conf file in it.',
    ),
  );
  return $items;
}

/**
 * Integration with VCS in order to easily commit your changes to projects.
 */
function dh_drush_engine_version_control() {
  return array(
    'git' => array(
      'signature' => 'git rev-parse --git-dir',
      'options' => array(
        '--version-control=git' => 'Quickly add/remove/commit your project changes to Git.',
        '  --gitsync' => 'Automatically add new files to the Git repository and remove deleted files. Caution.',
        '  --gitcommit' => 'Automatically commit changes to Git repository. You must also using the --gitsync option.',
        '  --gitmessage' => 'Override default commit message which is: Drush automatic commit: <the drush command line used>',
      ),
    ),
  );
}

/**
 * Command callback. Show a list of modules and status.
 *
 */
function drush_dh_make_drupal() {
  // Version 1 didn't specify an API version. Therefore if no
  // api parameter is set, assume the project intends to use the
  // version 1 API.
  if (!drush_get_option('dh-api', FALSE)) {
    drush_set_option('dh-api', 1);
  }

  // If the API versions don't match, don't continue.
  if (drush_get_option('dh-api') != DRUSH_DH_MAKE_API) {
    drush_set_error("Drush dh-make API version mismatch");
    drush_print("This version of drush dh-make implements API version " . DRUSH_DH_MAKE_API, 2);
    drush_print("It doesn't look like this project supports this version of the API.", 2);
    drush_print("To build this package with the correct API version try the following commands then try again:", 2);
    drush_print("");
    drush_print("pushd " . dirname(__FILE__), 4);
    drush_print("git checkout -b 7.x-" . drush_get_option('dh-api') . ".x --track origin/7.x-" . drush_get_option('dh-api') . '.x', 4);
    drush_print("popd", 4);
    drush_print("");
    drush_print("However, if you know this project does support version " . DRUSH_DH_MAKE_API . " of the dh-make API,", 2);
    drush_print("simply add --dh-api=" . DRUSH_DH_MAKE_API . " to your command and try again or add it to your drushrc file:", 2);
    drush_print("");
    drush_print("\$options['dh-api'] = " . DRUSH_DH_MAKE_API . ";", 4);
    return FALSE;
  }

  include_once(dirname(__FILE__) . '/dh.inc');
  include_once(dirname(__FILE__) . '/dh.debian.inc');
  // Check for an existing debian folder. If it exists, don't build.
  // For legacy reasons we don't attempt to remove the directory.
  if (is_dir(DRUPAL_ROOT . '/debian')) {
    return drush_set_error(DRUPAL_ROOT . '/debian already exists. Please remove or rename to .debian.');
  }

  $options = array('general' => array());

  // Before building the project, check if all the required modules
  // are enabled. These modules are listed as dependancies in the site
  // drushrc.php file.
  $depends = drush_get_option('dh-make-depends', array());
  foreach ($depends as $module) {
    if (!module_exists($module)) {
      drush_set_error($module . " is not enabled but is a dependancy of this build.");
    }
  }
  if (drush_get_error()) {
    return;
  }

  // Same as dependancies but with warnings only and are not fatal.
  $recommends = drush_get_option('dh-make-recommends', array());
  foreach ($recommends as $module) {
    if (!module_exists($module)) {
      drush_log($module . " is not enabled but is recommended for this build.", 'warning');
    }
  }

  // Check for module conflicts that should be disabled before build.
  $conflicts = drush_get_option('dh-make-conflicts', array());
  $continue = TRUE;
  foreach ($conflicts as $module) {
    if (module_exists($module)) {
      drush_log($module . " is enabled but is a conflict of this build.", 'warning');
      $continue = FALSE;
    }
  }
  if (!$continue) {
    return;
  }

  // Set version control system to git. Its the only driver this system
  // will work with.
  if (drush_get_option('version-control', FALSE) && drush_get_option('version-control', 'git') != 'git') {
    return drush_set_error("Cannot use VCS " . drush_get_option('version-control', 'git') . ' with dh-make. Only Git is supported.');
  }
  drush_set_option('version-control', 'git');

  // Try load a version control engine.
  if ($vc_engine = drush_pm_include_version_control(DRUPAL_ROOT)) {
    if (is_file(DRUPAL_ROOT . '/.gitmodules')) {
      return drush_set_error("Cannot build package: Git submodules are not supported.");
    }
  }
  else {
    return drush_set_error("A VCS (Git) is required to be able to build a debian package.");
  }

  // If there are any modified files under version control
  // uncommitted, do not build the package.
  $status = $vc_engine->status();
  foreach ($status as $idx => $file_state) {
    if (substr($file_state, 0, 2) == '??') {
      unset($status[$idx]);
    }
  }
  if (count($status)) {
    return drush_set_error("There are uncommitted files under the version control system. Please commit these files to version control and try build the package again.");
  }

  // Set user
  if (!drush_get_option('dh-user')) {
    if (!$vc_engine || (!$name = $vc_engine->getUsername())) {
      $name = drush_prompt("What is your name?");
    }
    drush_set_option('dh-user', $name);
  }

  // Set email
  if (!drush_get_option('dh-email')) {
    if (!$vc_engine || (!$email = $vc_engine->getEmail())) {
      $email = drush_prompt("What is your email?", drush_get_option('dh-user') . '@drupal.org');
    }
    drush_set_option('dh-email', $email);
  }

  // Set project name.
  if (!drush_get_option('dh-project')) {
    $default = array_pop(explode('/', DRUPAL_ROOT));
    $name = drush_prompt("What is the name of the Drupal project?", $default);
    drush_set_option('dh-project', $name);
  }

  if (!drush_get_option('dh-package')) {
    drush_set_option('dh-package', 'drupal-site-' . drush_get_option('dh-project'));
  }
  drush_set_option('dh-build-dir', '/tmp/' . drush_get_option('dh-package'));

  // Set project web root.
  if (!drush_get_option('wwwroot')) {
    drush_set_option('wwwroot', '/var/www/' . drush_get_option('dh-package'));
  }
  drush_set_option('wwwrootns', substr(drush_get_option('wwwroot'), 1));

  // Change the version in the debian change log and tag it in VCS.
  if (!is_dir(DRUPAL_ROOT . '/.debian') && !mkdir(DRUPAL_ROOT . '/.debian')) {
    return drush_set_error("Cannot create .debian directory");
  }

  // Give modules the option to add any further files to the .debian directory.
  module_invoke_all('dh_make_prebuild', DRUPAL_ROOT . '/.debian');

  // If we're running version control, it would be a nice idea to build
  // the debian package from version control rather than from the existing
  // directory because this current dir may include information we don't want.
  dh_set_root($vc_engine, $vc_engine->getBranch());
  drush_set_option('dh-git-tag', $vc_engine->getBranch());

  if (!dh_create_debian()) {
    return FALSE;
  }

  // With the debian folder in place and the placheholders filled up, its now
  // time to attempt to build the debian package.
  $dir = exec("pwd");
  drush_print("Building debian package. This may take a few seconds....");
  chdir(drush_get_option('dh-build-dir'));
  if (drush_shell_exec("debuild -i -us -uc -b")) {
    drush_log(dt("Debian package completed"));
    // We can assume the package was build in the directory above with a standard naming schema.
    // drupal-site-[project]_[date]-[idx]_add.deb Lets try find it.
    $deb = drush_get_option('dh-build-dir') . '/../' . drush_get_option('dh-package') . '_' . drush_get_option('dh-version') . '_all.deb';
    chdir($dir);
  }
  else {
    chdir($dir);
    return drush_set_error("Debian package failed to build. Run --debug to see full errors.");
  }

  // This only occurs when the version has been bumped.
  if ($vc_engine) {
    $vc_engine->commit(array('path' => DRUPAL_ROOT));
    $tag = $vc_engine->getBranch() . '/' . drush_get_option('dh-package') . '_' . drush_get_option('dh-version');
    $vc_engine->tag($tag);
    if (!$remote = drush_get_option('git-push-tag', FALSE)) {
      drush_shell_exec("cd %s && git remote", DRUPAL_ROOT);
      $options = array();
      foreach (drush_shell_exec_output() as $line) {
        $options[] = array('!key' => $line);
      }
      $remote = drush_choice($options, "Which remote should dh-make-drupal push tags too?");
      $remote = isset($options[$remote]) ? $options[$remote]['!key'] : FALSE;
    }
    if ($remote) {
      drush_set_option('git-push-tag', $remote);
      drush_shell_exec_interactive("cd %s && git push %s %s", DRUPAL_ROOT, $remote, $tag);
    }
  }
  dh_recommend_drushrc();
  if ($deb = realpath($deb)) {
    drush_print(dt('Debian package build at !file', array('!file' => $deb)));
  }
  drush_command_invoke_all('debuild_postbuild', $deb);
}
